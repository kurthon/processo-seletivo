package com.mercadinho.repository;

import org.springframework.data.repository.CrudRepository;

import com.mercadinho.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, String>{
	Produto findByCodigo(long codigo);

	

}
